# Kali Linux dwm

## Table of Contents
- [Introduction](#introduction)
- [Pictures](#pictures)
- [Installation](#installation)
  - [Downloading](#downloading)
  - [Building](#building)
    - [Kali Debian Based Systems](#kali-debian-based-systems)
    - [Non-Kali Debian Based Systems](#non-kali-debian-based-systems)
- [Known Issues](#known-issues)
- [Bug Reports](#bug-reports)
- [Dotfiles](#dotfiles)
- [Credits](#credits)

## Introduction
This is Kali, using dwm as a WM. I've done this because dwm is fast and light and works best on my old chromebook.

---

## Pictures
![1][1]

![5][5]

![2][2]

![3][3]

![4][4]

---

## Installation
### Downloading

- [Kali dwm ISO][Kali dwm ISO]
- [Kali dwm Build Log][Kali dwm Build Log]

However, due constrains, the only version of the available image is `amd64`.
Thus,  if you require `i386` version of Kali dwm,
please refer to [building](#building).

### Building
#### Kali Debian Based Systems
If you choose to build the ISO instead of using the pre-built images available
[here][Kali i3 ISO], run the following commands:

```
$ sudo apt install -y curl git live-build cdebootstrap
$ git clone https://gitlab.com/mr00k3/kali-dwm.git
$ cd kali-dwm
$ ./build.sh --variant dwm --verbose
```

By default, the command above will build a Kali i3 ISO based on your host's
architecture. If you want another architecture (`i386`), use the `--arch` flag:

```
$ ./build.sh --arch <i386/amd64> --variant dwm --verbose
```

#### Non-Kali Debian Based Systems
If you are on a non-Kali Debian based system, check out Kali's
[documentation][Kali Docs] on building the ISO on Non-Kali Debian Based
Systems.

### **Once the script is finished, your image should be in `./images`.**

---

## Known Issues
- Firefox opacity i will fix it in future
- If not booting from `Ventoy` in `normal mode` on some machines you must use `grub2 mode`
- In VMs, (tested on `VMware`) `backend = "glx";` (Line 119 in `picom.conf`)
causes the VMs to act erratically cause lag (e.g., keyboard to not function
properly, etc.). To fix this, enable 3D acceleration for the VM in your
virtualization software of choice.
- Once the ISO is built and ran/installed, there won't be user directories (in
`$HOME` or in `root`). This is because `xdg-user-dirs-update` doesn't run 
(during build, live-system or install phase). A manual fix for this is to run
`xdg-user-dirs-update` in a terminal once the you log into the system.

---

## Bug Reports
If you find bug

- Fork the repository
- Make changes
- Submit a PR

I'll evaluate the changes ASAP and get back to you.

---

## Dotfiles
All of the default customizations I've created for Kali dwm can be found in this places
- On my github: [mr00k3/kali-dwm-dotfiles][mr00k3/kali-dwm-dotfiles]
- In `.config` on kali-dwm

---

## Credits
- [Arszilla] for [Kali i3] and dotfiles
- [Kali Team][Kali Team] for their work on
[live-build-config][live-build-config], making this ISO and project possible.
- [suckless] for [dwm], [st], [slstatus]
- [bakkeby] for [dwm-flexipatch]

[1]:                              /screenshots/kali-dwm-1.png
[2]:                              /screenshots/kali-dwm-2.png
[3]:                              /screenshots/kali-dwm-3.png
[4]:                              /screenshots/kali-dwm-4.png
[5]:                              /screenshots/kali-dwm-5.png
[Kali dwm ISO]:                   https://mega.nz/file/9uFxWLxJ#apa5BvkfbPa8bE4pAm2JrJghN3VqvZwGJCuibUxy0WM
[Kali dwm Build Log]:             https://mega.nz/file/0ydiEZDJ#RGtwZsa_VnVW1UbUQB7ixtt5y75jkAadV68UMzKR0k8
[mr00k3/kali-dwm-dotfiles]:       https://github.com/mr00k3/kali-dwm-dotfiles
[Kali Docs]:                      https://www.kali.org/docs/development/live-build-a-custom-kali-iso/
[Kali Team]:                      https://www.kali.org/about-us/
[live-build-config]:              https://gitlab.com/kalilinux/build-scripts/live-build-config
[bakkeby]:                        https://github.com/bakkeby
[dwm-flexipatch]:                 https://github.com/bakkeby/dwm-flexipatch
[Arszilla]:                       https://gitlab.com/Arszilla/
[Kali i3]:                        https://gitlab.com/Arszilla/kali-i3
[suckless]:                       https://suckless.org/
[dwm]:                            https://dwm.suckless.org/
[st]:                             https://st.suckless.org/
[slstatus]:                       https://tools.suckless.org/slstatus/